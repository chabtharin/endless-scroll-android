package com.chabtharin.endlessscrollandroid;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.chabtharin.endlessscrollandroid.adapter.ItemAdapter;
import com.chabtharin.endlessscrollandroid.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    private List<String> mItems = new ArrayList<>();
    private static final String TAG = "MainActivity";
    private ItemAdapter itemAdapter;
    boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        populateData();
        initAdapter();
        onInitScrollListener();

        binding.swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.i(TAG, "onRefresh: ");
                mItems.clear();
                itemAdapter.notifyDataSetChanged();
                binding.swipeLayout.setRefreshing(false);
                populateData();
            }
        });

    }

    private void initAdapter() {
        itemAdapter = new ItemAdapter(mItems);
        binding.rcItem.setAdapter(itemAdapter);
        binding.rcItem.setLayoutManager(new LinearLayoutManager(this));
    }

    private void populateData() {
        int i = 0;
        while (i < 10) {
            mItems.add("Item " + i);
            i++;
        }
    }

    private void onInitScrollListener() {
        binding.rcItem.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) binding.rcItem.getLayoutManager();
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == mItems.size() - 1) {
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }

    private void loadMore() {
        mItems.add(null);
        itemAdapter.notifyItemInserted(mItems.size() - 1);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mItems.remove(mItems.size() - 1);
                int scrollPosition = mItems.size();
                Log.d(TAG, "run: " + scrollPosition);
                itemAdapter.notifyItemRemoved(scrollPosition);
                int currentSize = scrollPosition;
                int nextLimit = currentSize + 10;

                while (currentSize - 1 < nextLimit) {
                    mItems.add("Item " + currentSize);
                    currentSize++;
                }
                Log.d(TAG, "run: " + mItems);
                itemAdapter.notifyDataSetChanged();
                isLoading = false;
            }
        }, 2000);


    }
}